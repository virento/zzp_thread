package com.tt.concurrent.callable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 1. Stwórz pulę wątków
 * a) Wykorzystaj Runtime.getRuntime().availableProcessors() aby pobrać ilość dostępnych procesów
 * b) pulę stwórz wykorzystując Executors.newFixedThreadPool(numberOfThreads);
 * 2. Stwórz zadania ComplexMathCallable w zależności od ilości wątków
 * 3. Zgłoś zadania do wykonania (metoda submit);
 * 4. Odłóż zwracane obiekty typu Future na listę - przydadzą się przy pobieraniu wyników
 * 5. Przeiteruj po liście i wywołaj metodę get() na obiektach Future
 * a) pamiętaj, że poszczególne wyniki należy zsumować
 * 6. Pamiętaj o zamknięciu ExecutorService'u - executor.shutdown();
 * 7. Zaprezentuj wyniki i zinterpretuj czas wykonania
 */
public class ComplexMathRunner {
    public static void main(String[] args) {

        final int LENGTH = 8000;
        ComplexMath math = new ComplexMath(LENGTH, LENGTH);
        List<Future<Double>> futureList = new ArrayList<>();
        int availableProcessors = Runtime.getRuntime().availableProcessors();

        System.out.println("Multithreaded calculation");
        System.out.println("Available processors: " + availableProcessors);
        ExecutorService executorService = Executors.newFixedThreadPool(availableProcessors);

        long startTime = System.currentTimeMillis();
        final int part = LENGTH / availableProcessors;
        for (int i = 0, done = 0; i < availableProcessors; i++) {
            ComplexMath complexMath = new ComplexMath(0, 0);
            if (i != availableProcessors - 1) {
                complexMath.setMatrix(Arrays.copyOfRange(math.getMatrix(), done, done + part));
                done += part;
            } else {
                complexMath.setMatrix(Arrays.copyOfRange(math.getMatrix(), done, LENGTH));
            }
            futureList.add(executorService.submit(new ComplexMathCallable(complexMath)));
        }
        double result = 0d;
        for (Future<Double> doubleFuture : futureList) {
            try {
                result += doubleFuture.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Result: " + result);
        System.out.println("Execution time: " + ((System.currentTimeMillis() - startTime) / 1000d) + " seconds.");
        executorService.shutdown();


        System.out.println("\n\nSingle thread calculation");
        startTime = System.currentTimeMillis();
        System.out.println("Result: " + math.calculate());
        long endTime = System.currentTimeMillis();
        System.out.println("Execution time: " + ((endTime - startTime) / 1000d) + " seconds.");
    }

    private static class ComplexMathCallable implements Callable<Double> {

        private ComplexMath complexMath;

        public ComplexMathCallable(ComplexMath math) {
            this.complexMath = math;
        }


        @Override
        public Double call() throws Exception {
            return complexMath.calculate();
        }
    }
}
